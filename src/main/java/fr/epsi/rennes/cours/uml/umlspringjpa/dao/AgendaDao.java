package fr.epsi.rennes.cours.uml.umlspringjpa.dao;

import fr.epsi.rennes.cours.uml.umlspringjpa.entity.Agenda;
import fr.epsi.rennes.cours.uml.umlspringjpa.entity.Contact;
import fr.epsi.rennes.cours.uml.umlspringjpa.exception.DaoException;

import java.util.List;

public interface AgendaDao {
    /**
     * Get an agenda by id
     *
     * @param id the id
     * @return the agenda
     * @throws DaoException
     */
    Agenda getById(Long id) throws DaoException;

    /**
     * Get an agenda by name
     *
     * @param name the name
     * @return the agenda
     * @throws DaoException
     */
    Agenda getByName(String name) throws DaoException;

    /**
     * Get a list of all agendas
     *
     * @return the list of all agendas
     * @throws DaoException
     */
    List<Agenda> getAll() throws DaoException;

    /**
     * Create a new agenda
     *
     * @param agenda the agenda to create
     * @throws DaoException
     */
    Agenda createAgenda(Agenda agenda) throws DaoException;

    /**
     * Update a agenda
     *
     * @param agenda the agenda to update
     * @throws DaoException
     */
    void updateAgenda(Agenda agenda) throws DaoException;

    /**
     * Delete a agenda
     *
     * @param agenda the agenda to delete
     * @throws DaoException
     */
    void deleteAgenda(Agenda agenda) throws DaoException;

    void addContactToAgenda(Agenda agenda, Contact contact) throws DaoException;
}
