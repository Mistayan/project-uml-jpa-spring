package fr.epsi.rennes.cours.uml.umlspringjpa;

import fr.epsi.rennes.cours.uml.umlspringjpa.dao.impl.AgendaDaoImpl;
import fr.epsi.rennes.cours.uml.umlspringjpa.dao.impl.ContactDaoImpl;
import fr.epsi.rennes.cours.uml.umlspringjpa.dao.impl.UserDaoImpl;
import fr.epsi.rennes.cours.uml.umlspringjpa.entity.*;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Persistence;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Getter
@Service
public class TestJpa {
    @Autowired
    EntityManager em;

    UserDaoImpl userDao;

    ContactDaoImpl contactDao;

    AgendaDaoImpl agendaDao;
    User testUser;
    Agenda testAgenda;
    Contact testContact;
    Email testEmail;
    Phone testPhone;
    Address testAddress;


    public TestJpa(Object o) {
        Map<String, String> properties = new HashMap<>();
        properties.put("spring.jpa.hibernate.ddl-auto", "create-drop");
        properties.put("spring.jpa.properties.hibernate.dialect", "org.hibernate.dialect.H2Dialect");
        properties.put("spring.datasource.url", "jdbc:h2:mem:testdb");
        properties.put("spring.datasource.driverClassName", "org.h2.Driver");
        properties.put("logging.level.org.hibernate.SQL", "DEBUG");
        properties.put("logging.level.org.hibernate.type.descriptor.sql.BasicBinder", "TRACE");
        this.em = Persistence.createEntityManagerFactory("crm-test").createEntityManager();
    }

    public TestJpa() {
        Map<String, String> properties = new HashMap<>();
        properties.put("spring.jpa.hibernate.ddl-auto", "create-drop");
        properties.put("logging.level.org.hibernate.SQL", "DEBUG");
        properties.put("logging.level.org.hibernate.type.descriptor.sql.BasicBinder", "TRACE");
        properties.put("spring.jpa.properties.hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");
        properties.put("spring.datasource.url", "jdbc:postgresql://localhost:5432/test");
        properties.put("spring.datasource.driverClassName", "org.postgresql.Driver");
        this.em = Persistence.createEntityManagerFactory("crm", properties).createEntityManager();
        this.userDao = new UserDaoImpl(em);
        this.contactDao = new ContactDaoImpl(em);
        this.agendaDao = new AgendaDaoImpl(em);
    }

    public static void main(String[] args) {
        TestJpa testJpa = new TestJpa();
        System.out.println(testJpa.addUser());
        System.out.println(testJpa.addContact());
        System.out.println(testJpa.addAddressToContact());
        System.out.println(testJpa.addEmailToContact());
        System.out.println(testJpa.addPhoneToContact());
        System.out.println(testJpa.deleteContact());
        System.out.println(testJpa.deleteUsers());
    }

    public Map<String, String> addUser() {
        Map<String, String> response = new HashMap<>();

        // create user
        User newUser = new User();
        newUser.setUsername("test");
        newUser.setPassword("test1243/%");
        newUser.setMail("test@tt.com");
        newUser.setGrants("admin");
        newUser.setActive(true);
        this.testUser = newUser;
        //persist User
        response.put("userIn", newUser.toString());
        try {
            this.testUser = userDao.createUser(newUser);
            this.testAgenda = userDao.getAgendaOfUser(testUser);
        } catch (Exception e) {
            e.printStackTrace();
        }
        response.put("userOut", this.testUser.toString());
        response.put("agendaOut", this.testAgenda.toString());
        return response;
    }

    public Map<String, String> addContact() {
        Map<String, String> response = new HashMap<>();

        // create a Contact and put it in the agenda
        Contact contact = new Contact();
        contact.setFirstName("toto");
        contact.setLastName("titi");
        // persist Contact
        response.put("contactIn", contact.toString());
        try {
            contactDao.createContact(contact);
            agendaDao.addContactToAgenda(testAgenda, contact);
            agendaDao.updateAgenda(testAgenda);
            this.testContact = contactDao.getContact(contact);
        } catch (Exception e) {
            e.printStackTrace();
        }
        response.put("contactOut", this.testContact.toString());
        return response;
    }

    public Map<String, String> addAddressToContact() {
        Map<String, String> response = new HashMap<>();

        Address address = new Address("15b rue de la paix", "Paris", "France", "91000");
        response.put("addressIn", address.toString());
        try {
            contactDao.addAddressToContact(testContact, address);
            contactDao.updateContact(testContact);
            List<Address> addresses = contactDao.getAddressesOfContact(testContact);
            response.put("addressOut", addresses.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public Map<String, String> addEmailToContact() {
        Map<String, String> response = new HashMap<>();
        Email email = new Email("contact.mail@google.com", "work");
        if (response.get("emailIn") != null) {
            //incerement key name
            int i = 1;
            while (response.get("emailIn" + i) != null) {
                i++;
            }
            response.put("emailIn" + i, email.toString());
        } else {
            response.put("emailIn", email.toString());
        }

        try {
            contactDao.addEmailToContact(testContact, email);
            contactDao.updateContact(testContact);
            List<Email> emails = contactDao.getEmailsOfContact(testContact);
            response.put("EmailOut", emails.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public Map<String, String> addPhoneToContact() {
        Map<String, String> response = new HashMap<>();
        Phone phone = new Phone("0123456789", "work");
        if (response.get("phoneIn") != null) {
            //incerement key name
            int i = 1;
            while (response.get("phoneIn" + i) != null) {
                i++;
            }
            response.put("phoneIn" + i, phone.toString());
        } else {
            response.put("phoneIn", phone.toString());
        }
        try {
            contactDao.addPhoneToContact(testContact, phone);
            contactDao.updateContact(testContact);
            List<Phone> phones = contactDao.getPhonesOfContact(testContact);
            response.put("phoneOut", phones.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public Map<String, String> deleteContact() {
        Map<String, String> response = new HashMap<>();
        Contact contact = contactDao.getContact(testContact);
        response.put("delete", contact.toString());
        try {
            contactDao.deleteContact(contact);
            assert contactDao.getContact(contact) != null;
            response.put("delete response", "Contact not deleted");
        } catch (Exception e) {
            response.put("delete response", "Contact deleted");
        }
        return response;
    }

    public Map<String, String> deleteUsers() {
        Map<String, String> response = new HashMap<>();
        response.put("delete contact", testContact.toString());
        String content = "";
        try {
            userDao.deleteUser(testUser);
            content = "User deleted";
        } catch (Exception e) {
            content = "User not deleted";
        }
        response.put("delete contact response", content);
        return response;
    }
}
