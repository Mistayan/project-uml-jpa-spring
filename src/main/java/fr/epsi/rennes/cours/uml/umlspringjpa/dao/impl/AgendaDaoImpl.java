package fr.epsi.rennes.cours.uml.umlspringjpa.dao.impl;

import fr.epsi.rennes.cours.uml.umlspringjpa.dao.AgendaDao;
import fr.epsi.rennes.cours.uml.umlspringjpa.entity.Agenda;
import fr.epsi.rennes.cours.uml.umlspringjpa.entity.Contact;
import fr.epsi.rennes.cours.uml.umlspringjpa.entity.User;
import fr.epsi.rennes.cours.uml.umlspringjpa.exception.DaoException;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import jakarta.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

@Transactional
public class AgendaDaoImpl implements AgendaDao {

    private final EntityManager em;
    Logger logger = LoggerFactory.getLogger(Agenda.class);

    public AgendaDaoImpl(EntityManager entityManager) {
        this.em = entityManager;
    }

    /**
     * @inheritDoc
     */
    @Override
    public Agenda getById(Long id) throws DaoException {
        try {
            return em.find(Agenda.class, id);
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    /**
     * @inheritDoc
     */
    @Override
    public Agenda getByName(String name) throws DaoException {
        try {
            Query query = em.createQuery("SELECT a FROM agendas a WHERE a.name = :name");
            query.setParameter("name", name);
            List<Agenda> agendas = query.getResultList();
            if (!agendas.isEmpty()) {
                return agendas.get(0);
            }
        } catch (Exception e) {
            throw new DaoException(e);
        }
        return null;
    }

    /**
     * @inheritDoc
     */
    @Override
    public List<Agenda> getAll() throws DaoException {
        try {
            Query query = em.createQuery("SELECT a FROM agendas a");
            return query.getResultList();
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    /**
     * @inheritDoc
     */
    @Override
    public Agenda createAgenda(Agenda agenda) throws DaoException {
        try {
            em.getTransaction().begin();
            em.persist(em.merge(agenda));
            em.getTransaction().commit();
            return agenda;
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }


    /**
     * @inheritDoc
     */
    @Override
    public void updateAgenda(Agenda agenda) throws DaoException {
        try {
            Agenda agendaToUpdate = em.find(Agenda.class, agenda.getId());
            agendaToUpdate.setId(agenda.getId());
            agendaToUpdate.setContacts(agenda.getContacts());
            em.getTransaction().begin();
            em.merge(agendaToUpdate);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    /**
     * Delete a agenda
     *
     * @param agenda the agenda to delete
     * @throws DaoException
     */
    @Override
    public void deleteAgenda(Agenda agenda) throws DaoException {
        try {
            Agenda agendaToDelete = em.find(Agenda.class, agenda.getId());
            em.remove(agendaToDelete);
            em.flush();
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    @Override
    public void addContactToAgenda(Agenda agenda, Contact contact) throws DaoException {
        try {
            em.getTransaction().begin();
            Agenda agendaToUpdate = em.find(Agenda.class, agenda.getId());
            agendaToUpdate.getContacts().add(contact);
            em.merge(agendaToUpdate);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    public void createAgendaForUser(User testUser) {
        Agenda agenda = new Agenda();
        agenda.setName(testUser.getUsername());
        agenda.setUser(testUser);
        em.getTransaction().begin();
        em.persist(em.merge(agenda));
        em.getTransaction().commit();
    }
}
