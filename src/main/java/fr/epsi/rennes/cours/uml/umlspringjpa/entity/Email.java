package fr.epsi.rennes.cours.uml.umlspringjpa.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "emails")
@Table(name = "emails", schema = "personal_agenda")
@NoArgsConstructor
public class Email implements ContactDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String address;
    private String type;

    @ManyToOne
    @JoinColumn(name = "contact_id")
    private Contact contact;

    public Email(String address, String type) {
        this.address = address;
        this.type = type;
    }

    @Override
    public String toString() {
        return "Email [id=" + id + ", address=" + address + "]";
    }

    @Override
    public void validate() {
        // email is already validated by JPA
    }
}