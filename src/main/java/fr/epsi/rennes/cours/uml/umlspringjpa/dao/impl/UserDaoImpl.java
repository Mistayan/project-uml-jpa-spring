package fr.epsi.rennes.cours.uml.umlspringjpa.dao.impl;

import fr.epsi.rennes.cours.uml.umlspringjpa.dao.AgendaDao;
import fr.epsi.rennes.cours.uml.umlspringjpa.dao.UserDao;
import fr.epsi.rennes.cours.uml.umlspringjpa.entity.Agenda;
import fr.epsi.rennes.cours.uml.umlspringjpa.entity.User;
import fr.epsi.rennes.cours.uml.umlspringjpa.exception.DaoException;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class UserDaoImpl implements UserDao {


    private final EntityManager em;
    private final AgendaDao agendaDao;
    Logger logger = LoggerFactory.getLogger(UserDaoImpl.class);

    public UserDaoImpl(EntityManager entityManager) {
        this.em = entityManager;
        this.agendaDao = new AgendaDaoImpl(entityManager);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public User getById(Long id) throws DaoException {
        try {
            return em.find(User.class, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new DaoException(e);
        }
    }

    /**
     * {@inheritDoc}
     * Utilisation d'une TypedQuery
     */
    @Override
    public User getByUsername(String username) throws DaoException {
        try {
            Query query = em.createQuery("SELECT u FROM users u WHERE u.username = :username");
            query.setParameter("username", username);
            List<User> users = query.getResultList();
            if (!users.isEmpty()) {
                return users.get(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new DaoException(e);
        }
        return null;
    }

    /**
     * {@inheritDoc}
     * Utilisation d'une TypedQuery
     */
    @Override
    public User getByUsernameAndPassword(String username, String password) throws DaoException {
        try {
            Query query = em.createQuery(
                    "SELECT u" +
                            " FROM users u" +
                            " WHERE u.username = :username" +
                            " AND u.password = :password");
            query.setParameter("username", username);
            query.setParameter("password", password);
            List<User> users = query.getResultList();
            if (!users.isEmpty()) {
                return users.get(0);
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            throw new DaoException(e);
        }
    }

    /**
     * {@inheritDoc}
     * Utilisation d'une TypedQuery
     */
    @Override
    public List<User> getAll() throws DaoException {
        try {
            Query query = em.createQuery("SELECT u FROM users u");
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            throw new DaoException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public User createUser(User user) throws DaoException {
        try {
            Agenda agenda = new Agenda();
            agenda.setName("Agenda de " + user.getUsername());
            em.getTransaction().begin();
            user.setAgenda(agenda);
            em.persist(user);
//            agendaDao.createAgenda(agenda);
            em.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            throw new DaoException(e);
        }
        return user;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateUser(User user) throws DaoException {
        try {
            em.getTransaction().begin();
            em.merge(user);
            em.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            throw new DaoException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteUser(User user) throws DaoException {
        try {
            em.getTransaction().begin();
            em.remove(user);
            em.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            throw new DaoException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public void addAgendaToUser(User user, Agenda agenda) throws DaoException {
        try {
            em.getTransaction().begin();
            user.setAgenda(agenda);
            em.merge(user);
            em.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            throw new DaoException(e);
        }
    }

    public Agenda getAgendaOfUser(User testUser) {
        Agenda agenda;
        try {
            em.getTransaction().begin();
            User user = em.find(User.class, testUser.getId());
            agenda = user.getAgenda();
            em.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            throw new DaoException(e);
        }
        return agenda;
    }
}
