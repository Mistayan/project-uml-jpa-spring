-- test user
SAVEPOINT user_interaction_emulation ;
INSERT INTO users (1, 'armimi', 'acme12', 'admin', t, 1);
INSERT INTO agendas (1, 'pda', 1);
INSERT INTO contacts (1, 'bob', 'bricoleur', 1);
INSERT INTO contacts (2, 'orel', 'architecte', 1);
INSERT INTO contacts (3, 'noe', 'plombier', 1);
INSERT INTO contacts (4, 'erwann', 'scientifique fou', 1);
INSERT INTO contacts (5, 'magali', 'cheffe pirate', 1);

COMMIT ;
