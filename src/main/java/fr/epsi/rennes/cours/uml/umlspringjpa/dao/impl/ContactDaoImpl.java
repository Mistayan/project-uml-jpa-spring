package fr.epsi.rennes.cours.uml.umlspringjpa.dao.impl;

import fr.epsi.rennes.cours.uml.umlspringjpa.dao.ContactDao;
import fr.epsi.rennes.cours.uml.umlspringjpa.entity.Address;
import fr.epsi.rennes.cours.uml.umlspringjpa.entity.Contact;
import fr.epsi.rennes.cours.uml.umlspringjpa.entity.Email;
import fr.epsi.rennes.cours.uml.umlspringjpa.entity.Phone;
import fr.epsi.rennes.cours.uml.umlspringjpa.exception.DaoException;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import jakarta.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

@Transactional
public class ContactDaoImpl implements ContactDao {


    private final EntityManager em;
    Logger logger = LoggerFactory.getLogger(ContactDaoImpl.class);

    public ContactDaoImpl(EntityManager entityManager) {
        this.em = entityManager;
    }


    /**
     * @inheritDoc
     */
    @Override
    public Contact getById(int id) throws DaoException {
        System.out.println("ContactDaoImpl.getById");
        System.out.println("id = " + id);
        try {
            return em.find(Contact.class, id);
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    /**
     * @inheritDoc
     */
    @Override
    public List<Contact> getAll() throws DaoException {
        try {
            Query query = em.createQuery("SELECT c FROM contacts c");
            return query.getResultList();
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    /**
     * @inheritDoc
     */
    @Override
    public Contact getContact(Contact contact) throws DaoException {
        try {
            Query query = em.createQuery("SELECT c FROM contacts c WHERE c.firstName = :firstname AND c.lastName = :lastname");
            query.setParameter("firstname", contact.getFirstName());
            query.setParameter("lastname", contact.getLastName());
            List<Contact> contacts = query.getResultList();
            if (!contacts.isEmpty()) {
                return contacts.get(0);
            }
        } catch (Exception e) {
            throw new DaoException(e);
        }
        return null;
    }

    /**
     * @inheritDoc
     */
    @Override
    public Contact createContact(Contact contact) throws DaoException {
        try {
            em.getTransaction().begin();
            em.persist(em.merge(contact));
            em.getTransaction().commit();
            return contact;
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    /**
     * @inheritDoc
     */
    @Override
    public void updateContact(Contact contact) throws DaoException {
        try {
            em.getTransaction().begin();
            em.persist(em.merge(contact));
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    /**
     * @inheritDoc
     */
    @Override
    public void deleteContact(Contact contact) throws DaoException {
        try {
            em.remove(contact);
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    /**
     * @inheritDoc
     */
    @Override
    public List<Contact> getByName(String name) throws DaoException {
        try {
            Query query = em.createQuery("SELECT c FROM contacts c WHERE :name = c.firstName OR :name = c.lastName");
            query.setParameter("name", name);
            List<Contact> contacts = query.getResultList();
            return contacts;
        } catch (Exception e) {
            throw new DaoException("No such name");
        }
    }

    /**
     * @inheritDoc
     */
    @Override
    public Contact getByEmail(String email) throws DaoException {
        try {
            Query query = em.createQuery("SELECT c FROM contacts c WHERE :email in elements(c.emails)");
            query.setParameter("email", email);
            List<Contact> contacts = query.getResultList();
            return contacts.get(0);
        } catch (Exception e) {
            throw new DaoException("No such email");
        }
    }

    /**
     * @inheritDoc
     */
    @Override
    public List<Contact> getByPhone(String phone) throws DaoException {
        try {
            Query query = em.createQuery("SELECT c FROM contacts c WHERE :phone MEMBER OF c.phones");
            query.setParameter("phone", phone);
            List<Contact> contacts = query.getResultList();
            return contacts;
        } catch (Exception e) {
            throw new DaoException("No such phone");
        }
    }

    /**
     * @inheritDoc
     */
    @Override
    public List<Contact> getByAddress(String address) throws DaoException {
        try {
            Query query = em.createQuery("SELECT c FROM contacts c WHERE :address in elements(c.addresses)");
            query.setParameter("address", address);
            List<Contact> contacts = query.getResultList();
            return contacts;
        } catch (Exception e) {
            throw new DaoException("No such address");
        }
    }

    @Override
    public void addAddressToContact(Contact contact, Address address) throws DaoException {
        try {
            em.getTransaction().begin();
            address.setContact(contact);
            em.persist(address);
            contact.getAddresses().add(address);
            em.persist(em.merge(contact));
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    @Override
    public void addPhoneToContact(Contact contact, Phone phone) throws DaoException {
        try {
            em.getTransaction().begin();
            phone.setContact(contact);
            em.persist(phone);
            contact.getPhones().add(phone);
            em.persist(em.merge(contact));
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    @Override
    public void addEmailToContact(Contact contact, Email email) throws DaoException {
        try {
            em.getTransaction().begin();
            email.setContact(contact);
            em.persist(email);
            contact.getEmails().add(email);
            em.persist(em.merge(contact));
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    @Override
    public List<Address> getAddressesOfContact(Contact contact) throws DaoException {
        try {
            Query query = em.createQuery("SELECT c FROM contacts c WHERE :contact = c");
            query.setParameter("contact", contact);
            List<Contact> contacts = query.getResultList();
            return contacts.get(0).getAddresses();
        } catch (Exception e) {
            throw new DaoException("No such contact");
        }
    }

    @Override
    public List<Phone> getPhonesOfContact(Contact contact) throws DaoException {
        try {
            Query query = em.createQuery("SELECT c FROM contacts c WHERE :contact = c");
            query.setParameter("contact", contact);
            List<Contact> contacts = query.getResultList();
            return contacts.get(0).getPhones();
        } catch (Exception e) {
            throw new DaoException("No such contact");
        }
    }

    @Override
    public List<Email> getEmailsOfContact(Contact contact) throws DaoException {
        try {
            Query query = em.createQuery("SELECT c FROM contacts c WHERE :contact = c");
            query.setParameter("contact", contact);
            List<Contact> contacts = query.getResultList();
            return contacts.get(0).getEmails();
        } catch (Exception e) {
            throw new DaoException("No such contact");
        }
    }

    @Override
    public void deleteAddressOfContact(Contact contact, Address address) throws DaoException {
        try {
            em.getTransaction().begin();
            contact.getAddresses().remove(address);
            em.merge(contact);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    @Override
    public void deletePhoneOfContact(Contact contact, Phone phone) throws DaoException {
        try {
            em.getTransaction().begin();
            contact.getPhones().remove(phone);
            em.merge(contact);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    @Override
    public void deleteEmailOfContact(Contact contact, Email email) throws DaoException {
        try {
            em.getTransaction().begin();
            contact.getEmails().remove(email);
            em.merge(contact);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    @Override
    public void updateAddressOfContact(Contact contact, Address address) throws DaoException {
        try {
            em.getTransaction().begin();
            contact.getAddresses().set(contact.getAddresses().indexOf(address), address);
            em.merge(contact);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    @Override
    public void updatePhoneOfContact(Contact contact, Phone phone) throws DaoException {
        try {
            em.getTransaction().begin();
            contact.getPhones().set(contact.getPhones().indexOf(phone), phone);
            em.merge(contact);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    @Override
    public void updateEmailOfContact(Contact contact, Email email) throws DaoException {
        try {
            contact.getEmails().set(contact.getEmails().indexOf(email), email);
            em.merge(contact);
            em.flush();
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }


}
