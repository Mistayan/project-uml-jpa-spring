package fr.epsi.rennes.cours.uml.umlspringjpa.controller;

import fr.epsi.rennes.cours.uml.umlspringjpa.TestJpa;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class UserController {

    @GetMapping("/test")
    public ResponseEntity<Map<String, String>> test() {
        Map<String, String> map = new HashMap<>();

        TestJpa testJpa = new TestJpa();
        map.putAll(testJpa.addUser());
        map.putAll(testJpa.addContact());
        map.putAll(testJpa.addEmailToContact());
        map.putAll(testJpa.addAddressToContact());
        map.putAll(testJpa.addPhoneToContact());
        map.putAll(testJpa.deleteContact());
        map.putAll(testJpa.deleteUsers());
        return ResponseEntity.ok(map);
    }
}
