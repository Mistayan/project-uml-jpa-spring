package fr.epsi.rennes.cours.uml.umlspringjpa.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "users")
@Table(name = "users", schema = "personal_agenda")
@NoArgsConstructor
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String username;
    private String password;
    @Email(message = "Email should be valid")
    private String mail;
    private String grants;  // "admin, user, guest"
    private boolean active;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "agenda_id")
    private Agenda agenda;

    @Override
    public String toString() {
        return "User [id=" + id + ", username=" + username + ", password=" + password + ", mail=" + mail + ", grants=" + grants + ", active=" + active + "]";
    }
}