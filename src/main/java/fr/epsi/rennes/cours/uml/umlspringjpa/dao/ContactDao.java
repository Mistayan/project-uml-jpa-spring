package fr.epsi.rennes.cours.uml.umlspringjpa.dao;

import fr.epsi.rennes.cours.uml.umlspringjpa.entity.Address;
import fr.epsi.rennes.cours.uml.umlspringjpa.entity.Contact;
import fr.epsi.rennes.cours.uml.umlspringjpa.entity.Email;
import fr.epsi.rennes.cours.uml.umlspringjpa.entity.Phone;
import fr.epsi.rennes.cours.uml.umlspringjpa.exception.DaoException;

import java.util.List;

public interface ContactDao {
    /**
     * Get a contact by id
     *
     * @param id the id
     * @return the contact
     * @throws DaoException not found
     */
    Contact getById(int id) throws DaoException;

    /**
     * Get a list of all contacts
     *
     * @return the list of all contacts
     * @throws DaoException not found
     */
    List<Contact> getAll() throws DaoException;

    /**
     * Create a new contact
     *
     * @param contact the contact to create
     * @throws DaoException failed to create
     */
    Contact createContact(Contact contact) throws DaoException;

    /**
     * get the specified contact
     */
    Contact getContact(Contact contact) throws DaoException;

    /**
     * Update a contact
     *
     * @param contact the contact to update
     * @throws DaoException failed to update
     */
    void updateContact(Contact contact) throws DaoException;

    /**
     * Delete a contact
     *
     * @param contact the contact to delete
     * @throws DaoException failed to delete
     */
    void deleteContact(Contact contact) throws DaoException;

    /**
     * Get a list of all contacts by name
     *
     * @param name the name
     * @return the list of all contacts
     * @throws DaoException not found
     */
    List<Contact> getByName(String name) throws DaoException;

    /**
     * Get a contact by email
     *
     * @param email the email
     * @return the list of all contacts
     * @throws DaoException not found
     */
    Contact getByEmail(String email) throws DaoException;

    /**
     * Get a list of all contacts by phone
     *
     * @param phone the phone
     * @return the list of all contacts
     * @throws DaoException not found
     */
    List<Contact> getByPhone(String phone) throws DaoException;

    /**
     * Get a list of all contacts by address
     *
     * @param address the address
     * @return the list of all contacts
     * @throws DaoException not found
     */
    List<Contact> getByAddress(String address) throws DaoException;

    /**
     * add address to contact
     *
     * @param contact contact to add the mail to
     * @param address address to add to contact
     * @throws DaoException failed to add
     */
    void addAddressToContact(Contact contact, Address address) throws DaoException;

    /**
     * add phone to contact
     *
     * @param contact contact to add the mail to
     * @param phone   phone to add to contact
     * @throws DaoException failed to add
     */
    void addPhoneToContact(Contact contact, Phone phone) throws DaoException;

    /**
     * add email to contact
     *
     * @param contact contact to add the mail to
     * @param email   email to add to contact
     * @throws DaoException failed to add
     */
    void addEmailToContact(Contact contact, Email email) throws DaoException;

    /**
     * get all addresses of a contact
     */
    List<Address> getAddressesOfContact(Contact contact) throws DaoException;

    /**
     * get all phones of a contact
     */
    List<Phone> getPhonesOfContact(Contact contact) throws DaoException;

    /**
     * get all emails of a contact
     */
    List<Email> getEmailsOfContact(Contact contact) throws DaoException;

    /**
     * delete address of a contact
     */
    void deleteAddressOfContact(Contact contact, Address address) throws DaoException;

    /**
     * delete phone of a contact
     */
    void deletePhoneOfContact(Contact contact, Phone phone) throws DaoException;

    /**
     * delete email of a contact
     */
    void deleteEmailOfContact(Contact contact, Email email) throws DaoException;

    /**
     * update address of a contact
     */
    void updateAddressOfContact(Contact contact, Address address) throws DaoException;

    /**
     * update phone of a contact
     */
    void updatePhoneOfContact(Contact contact, Phone phone) throws DaoException;

    /**
     * update email of a contact
     */
    void updateEmailOfContact(Contact contact, Email email) throws DaoException;

}
